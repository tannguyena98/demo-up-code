package main

import "fmt"

func main() {
	var i interface{} = 5
	s := i.(int)
	fmt.Println(s)
	a, ok := i.(int)
	fmt.Println(a, ok)
}

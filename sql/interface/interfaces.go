package main

import (
	"fmt"
)

type User interface {
	Work() string
}

func main() {
	var user User
	a := MyAge(20)
	b := Gender{true}
	user = &b
	user = a
	fmt.Println(user.Work())
}

type MyAge int32

func (a MyAge) Work() string {
	if a < 18 {
		return "Accept"
	}
	return "Refuse"
}

type Gender struct {
	X bool
}

func (g *Gender) Work() string {
	if g.X == true {
		return "Male"
	}
	return "Female"
}

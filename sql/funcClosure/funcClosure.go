package main

import "fmt"

func createAdder() func(int) int {
	sum := 0
	adder := func(x int) int {
		sum += x
		return sum
	}
	return adder
}
func main() {
	adder := createAdder()
	fmt.Println(adder(1))
	fmt.Println(adder(1))
	fmt.Println(adder(1))
	fmt.Println(adder(1))
}

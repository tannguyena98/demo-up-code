package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "123"
	dbname   = "2024"
	schema   = "public"
)

var connectionString = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
	host, port, user, password, dbname)

type Product struct {
	id         int
	category   string
	name       string
	price      int
	created_at string
}

func connectDB() (*sql.DB, error) {
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func createTable(db *sql.DB) {
	createProductTable := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS %s.Product(
		id serial primary key,
		category varchar(50),
		name varchar(100),
		price integer,
		created_at timestamp default current_timestamp
	)
	`, schema)

	_, err := db.Exec(createProductTable)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Create table successfully!")
}

func insert(db *sql.DB) {
	insertProduct := fmt.Sprintf(`
	insert into %s.Product (category, name, price, created_at)
	values ($1, $2, $3, current_timestamp)
	`, schema)

	productData := Product{
		category: "cake",
		name:     "cosi",
		price:    30000,
	}

	_, err := db.Exec(insertProduct, productData.category, productData.name, productData.price)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Insert values successfully!")
}
func update(db *sql.DB) {
	updateProduct := fmt.Sprintf(`
        update %s.Product
        set price = $1,
            name = $2
        where id = $3
    `, schema)

	newPrice := 111111
	newName := "New Name"
	_, err := db.Exec(updateProduct, newPrice, newName, 2)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Updated successfully data!")
}

func delete(db *sql.DB) {
	deleteProduct := fmt.Sprintf(`
	delete from %s.Product 
	where id = $1 
	`, schema)
	idToDelete := "2"
	_, err := db.Exec(deleteProduct, idToDelete)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Deleted successfully!")
}
func GetAll(db *sql.DB) {
	selectProducts := fmt.Sprintf(`
		select * from  %s.Product
		`, schema)
	rows, err := db.Query(selectProducts)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var products []Product
	for rows.Next() {
		var product Product
		err := rows.Scan(&product.id, &product.category, &product.name, &product.price, &product.created_at)
		if err != nil {
			log.Fatal(err)
		}
		products = append(products, product)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	for _, product := range products {
		fmt.Printf("Category: %v,\nName: %v, \nPrice: %v\n", product.category, product.name, product.price)
	}

}
func dropTable(db *sql.DB) {
	dropProductTable := fmt.Sprintf(`
		DROP TABLE IF EXISTS %s.Product;
	`, schema)

	_, err := db.Exec(dropProductTable)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("đã xoá bảng")
}

func main() {
	db, err := connectDB()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	delete(db)
}
